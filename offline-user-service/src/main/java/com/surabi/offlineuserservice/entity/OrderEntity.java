package com.surabi.offlineuserservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.surabi.offlineuserservice.model.Mode;
import com.surabi.offlineuserservice.model.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrderEntity implements Serializable {
    @Id
    @Column
    private String id;
    @Column
    private String items;
    @Column
    private Double bill;
    @Column
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column
    @Enumerated(EnumType.STRING)
    private Mode mode;
    @Column
    private String feedback;
    @Column(unique = true)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate event;
}
