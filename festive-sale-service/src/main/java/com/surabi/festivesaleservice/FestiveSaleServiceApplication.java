package com.surabi.festivesaleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FestiveSaleServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FestiveSaleServiceApplication.class, args);
    }

}
