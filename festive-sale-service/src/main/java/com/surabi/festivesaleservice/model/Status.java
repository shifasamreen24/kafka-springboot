package com.surabi.festivesaleservice.model;

public enum Status {
    UNPAID, PAID;
}
