package com.surabi.festivesaleservice.repository;

import com.surabi.festivesaleservice.entity.CouponEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<CouponEntity, String> {
}
