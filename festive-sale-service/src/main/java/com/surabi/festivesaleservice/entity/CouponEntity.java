package com.surabi.festivesaleservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CouponEntity implements Serializable {
    @Id
    @Column(unique = true)
    private String code;
    @Column
    private String name;
}
