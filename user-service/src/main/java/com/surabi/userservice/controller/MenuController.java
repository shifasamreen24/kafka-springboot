package com.surabi.userservice.controller;


import com.surabi.userservice.entity.MenuEntity;
import com.surabi.userservice.repository.MenuRepository;
import com.surabi.userservice.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class MenuController {

    @Autowired
    IMenuService iMenuService;

    @Autowired
    MenuRepository menuRepository;

    // select menu item by id
    @GetMapping(value = "/menu/item/{id}")
    @ApiOperation(value = "Get specific Menu Item", notes = "View menu item by its id")
    public Optional<MenuEntity> getMenuItemById(@PathVariable Long id){
        return menuRepository.findById(id);
    }

    //select menu items list of ids
    @GetMapping(value = "/menu/item")
    @ApiOperation(value = "Get Menu items", notes = "View menu items by id")
    public List<MenuEntity> getMenuItemsByIds(@RequestParam List<Long> ids){
        return menuRepository.findAllById(ids);
    }

    //get all menu items
    @GetMapping(value = "/menu")
    @ApiOperation(value = "Get All Menu items", notes = "Fetch all items from menu as list")
    public List<MenuEntity> getAllMenuItems(){
        return menuRepository.findAll();
    }

    //select menu and generate bill
    @PostMapping("/placeOrder")
    @ApiOperation(value = "Select from Menu", notes = "Fetch total bill amount")
    public Map<String, Double> generateBill(@RequestBody Collection<Long> ids){
        Double total_bill_amount= menuRepository.generateBill(ids);
        HashMap<String, Double> map = new HashMap<>();
        map.put("Bill Generated",total_bill_amount);
        return map;
    }

}
