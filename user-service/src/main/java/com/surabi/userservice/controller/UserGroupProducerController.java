package com.surabi.userservice.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class UserGroupProducerController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private Gson gson;

    @PostMapping("/post_to_user_group_chat")
    public ResponseEntity<String> postToUsersGroupChat (@RequestParam String messageToUsers ) throws InterruptedException, ExecutionException {
        ListenableFuture<SendResult<String, String>> resultListenableFuture = kafkaTemplate.send("users-group-chat-topic", gson.toJson(messageToUsers));
        return new ResponseEntity<>(resultListenableFuture.get().getProducerRecord().value(), HttpStatus.OK);
    }
}
