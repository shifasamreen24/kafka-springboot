package com.surabi.userservice.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class UserGroupConsumerController {

    @Autowired
    private Gson gson;

    @KafkaListener(topics = {"users-group-chat-topic"})
    public void receiveMessagesFromOnlineUser(@RequestParam String messageFromOnlineUser){
        log.info("Msg received from Kafka is: {}", messageFromOnlineUser);
        String message = gson.toJson(messageFromOnlineUser, String.class);
        log.info("Msg converted using gson: {}", message);
    }

}
