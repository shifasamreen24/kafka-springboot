package com.surabi.userservice.service.impl;


import com.surabi.userservice.entity.MenuEntity;
import com.surabi.userservice.repository.MenuRepository;
import com.surabi.userservice.service.IMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MenuServiceImpl implements IMenuService {

    @Autowired
    MenuRepository menuRepository;

    @Override
    public List<MenuEntity> getAllMenuItems() {
        return menuRepository.findAll();
    }


}
