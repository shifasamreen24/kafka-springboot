package com.surabi.userservice.model;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @NonNull
    private String username;
    @NonNull
    private String password;
    @NonNull
    private Boolean enabled;
}
