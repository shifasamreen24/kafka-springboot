package com.surabi.adminservice.controller;


import com.surabi.adminservice.entity.AuditEntity;
import com.surabi.adminservice.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AuditController {

    @Autowired
    AuditRepository auditRepository;

    @GetMapping("audit_logs")
    public List<AuditEntity> getAllOrdersLogs(){
        return auditRepository.findAll();
    }
    @GetMapping("/sale_this_month")
    public Double getTotalSalesThisMonth(){
        return auditRepository.totalSalesThisMonth();
    }
}
